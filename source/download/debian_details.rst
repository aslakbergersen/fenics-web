.. _debian_details:

########
Download
########

.. image:: images/debian.png
    :align: left

`Install FEniCS <apt://fenics>`__

#########################
Installation instructions
#########################

FEniCS is included as part of Debian GNU/Linux. To install, simply click
on the link above or run the following command in a terminal::

    sudo apt-get install fenics

<div id="main">
  <div id="container" class="feature">
    <div id="content">
      <div id="sub-feature">

      	<div class="front-block block app">

	  <h3>ASCoT &mdash; Automated Stability Condition Tester </h3>
          <img alt='' src='/_static/images/ascot.png'
          class='avatar avatar-84 photo' height='84' width='84' />

	  <p> ASCoT is a light-weight, FEniCS-based, Python module
          designed for examining and determining the numerical
          stability of finite element discretizations. Its
          functionality allows for identifying stable mixed finite
          element spaces for variational saddle point problems, for
          optimizing stabilization constants, and in general, for
          computing stability constants through series of eigenvalue
          problems.  </p>

          <p><a href="https://launchpad.net/ascot" rel="nofollow">Visit the
          project&#8217;s web site to learn more &raquo;</a></p>

        </div>

      	<div class="front-block block app">

	  <h3>CBC.Block &mdash; Block linear algebra in Python</h3>
          <img alt='' src='/_static/images/cbc_block.png'
          class='avatar avatar-84 photo' height='84' width='84' />

	  <p>CBC.Block is a FEniCS based Python library for block
             operators, operator algebra and algebraic solvers
             developed at the <a href="http://cbc.simula.no">Center
             for Biomedical Computing</a> hosted
             by <a href="http://www.simula.no">Simula Research
             Laboratory</a> in Oslo. CBC.Block provides a simple
             interface for specifying block-partitioned problems and
             preconditioners, and solving them.
          </p>

          <p><a href="https://bitbucket.org/fenics-apps/cbc.block" rel="nofollow">Visit the
          project&#8217;s web site to learn more &raquo;</a></p>

        </div>

      	<div class="front-block block app">

	  <h3>CBC.PDESys &mdash; Specify large systems of PDEs with ease</h3>
          <img alt='' src='/_static/images/featured/featured_item_pdesys.png'
          class='avatar avatar-84 photo' height='84' width='84' />

          <p> CBC.PDESys is a Python package for specifying and solving
          large, completely general, systems of nonlinear Partial Differential
          Equations (PDEs) with very compact, flexible and reusable code.
          CBC.CFD is a subpackage of CBC.PDESys that focuses on Computational
          Fluid Dynamics and turbulence modeling. CBC.CFD also contains some highly
          optimized Navier-Stokes solvers.
          </p>

          <p><a href="https://launchpad.net/cbcpdesys"
          rel="nofollow">Visit the project&#8217;s web site to learn more
          &raquo;</a></p>

        </div>

        <div class="front-block block app">

          <h3>CBC.Solve &mdash; A collection of Python solvers</h3>
          <img alt='' src='/_static/images/cbc_solve.png'
          class='avatar avatar-84 photo' height='84' width='84' />

	  <p>CBC.Solve is a collection of FEniCS based solvers
	     developed at the <a href="http://cbc.simula.no">Center
	     for Biomedical Computing</a> hosted
	     by <a href="http://www.simula.no">Simula Research
	     Laboratory</a> in Oslo. The current collection of solvers
	     includes CBC.Flow (Navier-Stokes), CBC.Twist
	     (hyperelasticity), CBC.Swing (fluid-structure
	     interaction) and CBC.Beat (bidomain equations).
           </p>

          <p><a href="https://launchpad.net/cbc.solve" rel="nofollow">Visit the
          project&#8217;s web site to learn more &raquo;</a></p>

        </div>

        <div class="front-block block app">

          <h3>dolfin-adjoint &mdash; A library for adjoining models</h3>
          <img alt='dolfin-adjoint' src='/_static/images/dolfinadjoint_tmp.png'
          class='avatar avatar-84 photo' height='84' width='84' />

	  <p> The main aim of dolfin-adjoint is to facilitate the
          automated development of reliable adjoint models. This
          Python library fits seamlessly on top of the FEniCS core
          components and offers easy-to-use functionality for
          automatically deriving and efficiently solving adjoint and
          tangent linear models starting from DOLFIN-based forward
          models.  </p>

          <p><a href="http://dolfin-adjoint.org"
          rel="nofollow">Visit the project&#8217;s web site to learn
          more &raquo;</a></p>

        </div>

      	<div class="front-block block app">

	  <h3>DOLFWAVE &mdash; A library for surface water waves</h3>
          <img alt='' src='/_static/images/dolfwave_tsunami.png'
          class='avatar avatar-84 photo' height='84' width='84' />

	   <p>DOLFWAVE is a FEniCS based  library for solving surface water waves
          problems such as Tsunami generation and propagation.
          It is developed at <a href="http://ptmat.fc.ul.pt/lab_analise_num.html">
          LAN </a> hosted by <a href="http://ptmat.fc.ul.pt"> CMAF </a> in Lisbon.
          The main goal of DOLFWAVE is to provide a framework for the analysis,
          development and computation of Boussinesq-type models.
          </p>

          <p><a href="https://launchpad.net/dolfwave"
          rel="nofollow">Visit the project&#8217;s web site to learn more
          &raquo;</a></p>

        </div>

      	<div class="front-block block app">

	  <h3>FEniCS Solid Mechanics &mdash; A solid mechanics library for FEniCS</h3>
          <img alt='' src='/_static/images/fenics_plasticity.png'
          class='avatar avatar-84 photo' height='84' width='84' />

	  <p>FEniCS Solid Mechanics aims at providing functionality to make it
      simpler for application developers to create solvers for complex solid
        mechanics problems such as plasticity. Currently, the Von Mises and
        Drucker-Prager plasticity models are implemented. However, the
        structure of the library should be general enough to allow other models
        to be added with little effort.
        </p>

          <p><a href="https://bitbucket.org/fenics-apps/fenics-solid-mechanics"
          rel="nofollow">Visit the project&#8217;s web site to learn more
          &raquo;</a></p>

        </div>

    	<div class="front-block block app">

	  <h3>GenFoo &mdash; A general Fokker-Planck solver</h3>
          <img alt='' src='/_static/images/gen_foo_rf_heated_ions.png'
          class='avatar avatar-84 photo' height='84' width='84' />

	  <p>GenFoo is a general Fokker-Planck solver for problems of arbitrary
          dimensionality, developed at <a href="http://www.kth.se">KTH</a>.
          It contains three backend solvers, a delta-f Monte Carlo, a standard
          Monte Carlo and a Finite Element solver.
          The key property of the GenFoo package is that physics is separated
          from numerics by runtime loading of the Fokker-Planck coefficients,
          which enable solutions of a large class of Fokker-Planck models.
          </p>

          <p><a href="https://launchpad.net/genfoo"
          rel="nofollow">Visit the project&#8217;s web site to learn more
          &raquo;</a></p>

        </div>

    	<div class="front-block block app">

	  <h3>Gryphon &mdash; A time integrator for FEniCS</h3>
          <img alt='' src='/_static/images/gryphon.png'
          class='avatar avatar-84 photo' height='84' width='84' />

	  <p>
          Gryphon is a Python module for solving systems of time
          dependent partial differential equations in FEniCS. Currently
          supported time-stepping methods include singly
          diagonally implicit Runge-Kutta methods with an explicit first stage
          (ESDIRKs) of order 2, 3 and 4, developed at the
          Norwegian University of Science and Technology. Several examples
          are included to illustrate how to get started with solving
          time dependent problems.
          </p>

          <p><a href="https://launchpad.net/gryphonproject"
          rel="nofollow">Visit the project&#8217;s web site to learn more
          &raquo;</a></p>

        </div>

    	<div class="front-block block app">

	  <h3>PUM solver &mdash; Partition of unity method in FEniCS</h3>
          <img alt='' src='/_static/images/crack_dolfin.png'
          class='avatar avatar-84 photo' height='84' width='84' />

	  <p>PUM solver is a FEniCS based package to model problems whose solutions
          are discontinuous across arbitrary surfaces using the partition of unity
          method (extended finite element method).
          The PUM solver consists of two components: a
          <a href="https://launchpad.net/dolfin-pum" rel="nofollow">PUM library</a>
          and a
          <a href="https://launchpad.net/ffc-pum" rel="nofollow">PUM compiler</a>.
          </p>
        </div>

    	<div class="front-block block app">

	  <h3>Unicorn &mdash; A massively parallel adaptive continuum solver</h3>
          <img alt='' src='/_static/images/unicorn_mixer.gif'
          class='avatar avatar-84 photo' height='84' width='84' />

          <p> Unicorn is a massively parallel adaptive finite element
          solver for fluid and structure mechanics, including FSI problems.
          Unicorn aims to be a unified continuum
          mechanics solver for a wide range of applications.
          Strong scaling has been demonstrated up to
          6000 cores. It is primarily developed by <a href="http://www.csc.kth.se/ctl">CTL</a>
          hosted at <a href="http://www.kth.se">KTH</a> in Stockholm.
          </p>

          <p><a href="https://launchpad.net/unicorn"
          rel="nofollow">Visit the project&#8217;s web site to learn more
          &raquo;</a></p>

        </div>
      </div><!-- #sub-feature -->
    </div><!-- #content -->
  </div><!-- #container .feature -->
</div><!-- #main -->
